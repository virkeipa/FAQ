## Bitergia Analytics FAQ

1. General
    1. [How to contact support?]
    1. [How to merge request to a repository?]
    1. [Set up your privacy settings]
    1. [Set up your notification preferences]
    1. [Structure of the Gitlab repos]
    1. [How does Bitergia handle feature requests?]
    1. [Which are the checks performed at Bitergia?]
    1. [How to add new members to your GitLab project?]
    1. [Which are your permissions on the GitLab repos?]
1. Identities
    1. [How to add a new organization?]
    1. [How to remove an organization?]
    1. [How to assign an e-mail domain to an organization?]
    1. [How to enroll a person to an organization during a time interval?]
    1. [How to prevent that affiliations removed from a contributor are restored?]
    1. [How to un-enroll a person from an organization?]
    1. [How to correctly enroll a person to different organizations during consecutive time periods avoiding overlapping dates?]
    1. [How to enroll a person to two organizations during the same time intervals?]
    1. [What are the implications when an identity has two enrollments with overlapping dates?]
    1. [How many types of matching do exist?]
    1. [Can I apply more matching algorithms?]
    1. [What is the difference between matching by email and email-name?]
    1. [How to blacklist an email via the identities file?]
    1. [How long does it take to see the changes in HatStall reflected in the dashboard?]
    1. [What is the organization JSON file?]
    1. [What is the identities file?]
    1. [What does it happen when merging two SortingHat databases?]
    1. [How to add a file of list of people?]
1. Data sources management
    1. [What is the projects.json?]
    1. [What is the setup.cfg?]
    1. [How do the setup.cfg and projects.json relate each other?]
    1. [How to add a new repository?]
    1. [How to add a new MBox repository?]
    1. [How to enrich only a part of the raw data?]
    1. [How to keep a repository in the projects.json when it doesn't exist anymore in upstream?]
    1. [How to add labels to repositories?]
    1. [How to enable the tracking of a Slack instance?]
    1. [How to get the ID of Slack channel?]
    1. [How to enable the download of archives in Groups.io?]
    1. [Which are the data sources that not allow retrieving all their history?]
1. Dashboards
    1. [How to login?]
    1. [How to check the data is up to date?]
    1. [I don't see any data in the dashboard, what do I do?]
    1. [Can't save my work, red error on top bar]
    1. [Can't share my work, red error on top bar]
    1. [How to use the dashboard Query bar?]
    1. [How to set my own filters?]
    1. [How to set a filter with painless code?]
    1. [What is an index pattern?]
    1. [What is an index?]
    1. [Where can we find a description of the different indexes?]
    1. [How to inspect an index from Kibiter?]
    1. [How to query elasticsearch from Kibiter?]
    1. [How to share a dashboard?]
    1. [How to import/export dashboards?]
    1. [How to properly save a new visualization or dashboard?]
    1. [How to edit a visualization?]
    1. [How to edit the name of a visualization?]
    1. [How to create a visualization?]
    1. [How to edit a dashboard?]
    1. [How to create a dashboard?]
    1. [How to change the top menu?]
    1. [Author_org_name and author_org_name, what's the difference?]
    1. [How to format a date?]
    1. [How to clear the kibana cache?]

[How to contact support?]:how-to-general/README.md#contact-support
[How to merge request to a repository?]:how-to-general/README.md#how-to-merge-request-to-a-repository
[Set up your Privacy settings]:how-to-general/README.md#privacy-settings
[Set up your Notification preferences]:how-to-general/README.md#setup-your-gitlab-notification-preferences
[Structure of the Gitlab repos]:how-to-general/README.md#structure-under-bitergiac
[How does Bitergia handle feature requests?]:how-to-general/README.md#how-does-bitergia-handle-feature-requests
[Which are the checks performed at Bitergia?]:how-to-general/README.md#which-are-the-checks-performed-at-bitergia
[How to add new members to your GitLab project?]:how-to-general/README.md#how-to-add-new-members-to-your-gitlab-project
[Which are your permissions on the GitLab repos?]:how-to-general/README.md#which-are-your-permissions-on-the-gitlab-repos
    
[How to add a new organization?]:how-to-identities/README.md#how-to-add-a-new-organization
[How to remove an organization?]:how-to-identities/README.md#how-to-remove-an-organization
[How to assign an e-mail domain to an organization?]:how-to-identities/README.md#how-to-assign-an-e-mail-domain-to-an-organization
[How to enroll a person to an organization during a time interval?]:how-to-identities/README.md#how-to-enroll-a-person-to-an-organization-during-a-time-interval    
[How to un-enroll a person from an organization?]:how-to-identities/README.md#how-to-un-enroll-a-person-from-an-organization
[How to correctly enroll a person to different organizations during consecutive time periods avoiding overlapping dates?]:how-to-identities/README.md#how-to-correctly-enroll-a-person-to-different-organizations-during-consecutive-time-periods-avoiding-overlapping-dates
[How to enroll a person to two organizations during the same time intervals?]:how-to-identities/README.md#how-to-enroll-a-person-to-two-organizations-during-the-same-time-intervals
[What are the implications when an identity has two enrollments with overlapping dates?]:how-to-identities/README.md#what-are-the-implications-when-an-identity-has-two-enrollments-with-overlapping-dates
[How many types of matching do exist?]:how-to-identities/README.md#how-many-types-of-matching-do-exist
[Can I apply more matching algorithms?]:how-to-identities/README.md#can-i-apply-several-matching-algorithms-together
[What is the difference between matching by email and email-name?]:how-to-identities/README.md#what-is-the-difference-between-matching-by-email-and-email-name
[How to blacklist an email via the identities file?]:how-to-identities/README.md#how-to-blacklist-an-email-via-the-identities-file
[What is the organization JSON file?]:how-to-identities/README.md#what-is-the-organization-json-file
[What is the identities file?]:how-to-identities/README.md#what-is-the-identities-file
[How long does it take to see the changes in HatStall reflected in the dashboard?]:how-to-identities/README.md#how-long-does-it-take-to-see-the-changes-in-hatstall-reflected-in-the-dashboard
[How to prevent that affiliations removed from a contributor are restored?]:how-to-identities/README.md#how-to-prevent-that-affiliations-removed-from-a-contributor-are-restored
[What does it happen when merging two SortingHat databases?]:how-to-identities/README.md#what-does-it-happen-when-merging-two-sortinghat-databases
[How to add a file of list of people?]:how-to-identities/README.

[What is the projects.json?]:how-to-sources/README.md#what-is-the-projectsjson
[What is the setup.cfg?]:how-to-sources/README.md#what-is-the-setupcfg
[How do the setup.cfg and projects.json relate each other?]:how-to-sources/README.md#how-do-the-setupcfg-and-projectsjson-relate-each-other
[How to add a new repository?]:how-to-sources/README.md#how-to-add-a-new-repository
[How to add a new MBox repository?]:how-to-sources/README.md#how-to-add-a-new-mbox-repository
[How to enrich only a part of the raw data?]:how-to-sources/README.md#how-to-enrich-only-a-part-of-the-raw-data
[How to keep a repository in the projects.json when it doesn't exist anymore in upstream?]:how-to-sources/README.md#how-to-keep-a-repository-in-the-projectsjson-when-it-doesnt-exist-anymore-in-upstream
[How to add labels to repositories?]:how-to-sources/README.md#how-to-add-labels-to-repositories
[How to enable the tracking of a Slack instance?]:how-to-sources/README.md#how-to-enable-the-tracking-of-a-slack-instance
[How to get the ID of Slack channel?]:how-to-sources/README.md#how-to-get-the-id-of-slack-channel
[How to enable the download of archives in Groups.io?]:how-to-sources#how-to-enable-the-download-of-archives-in-groupsio
[Which are the data sources that not allow retrieving all their history?]:how-to-sources/README.md#which-are-the-data-sources-that-not-allow-retrieving-all-their-history

[How to login?]:how-to-dashboard/README.md#how-to-login
[How to check the data is up to date?]:how-to-dashboard/README.md#how-to-check-the-data-is-up-to-date
[I don't see any data in the dashboard, what do I do?]:how-to-dashboard/README.md#i-dont-see-any-data-in-the-dashboard-what-do-i-do
[Can't save my work, red error on top bar]:how-to-dashboard/README.md#cant-save-my-work-red-error-on-top-bar
[Can't share my work, red error on top bar]:how-to-dashboard/README.md#cant-share-my-work-red-error-on-top-bar
[How to use the dashboard Query bar?]:how-to-dashboard/README.md#how-to-use-the-dashboard-query-bar
[How to set my own filters?]:how-to-dashboard/README.md#how-to-set-my-own-filters
[How to set a filter with painless code?]:how-to-dashboard/README.md#how-to-set-a-filter-with-painless-code
[What is an index pattern?]:how-to-dashboard/README.md#what-is-an-index-pattern
[What is an index?]:how-to-dashboard/README.md#what-is-an-index
[Where can we find a description of the different indexes?]:how-to-dashboard/README.md#where-can-we-find-a-description-of-the-different-indexes
[How to inspect an index from Kibiter?]:how-to-dashboard/README.md#how-to-inspect-an-index-from-kibiter
[How to query elasticsearch from Kibiter?]:how-to-dashboard/README.md#how-to-query-elasticsearch-from-kibiter
[How to share a dashboard?]:how-to-dashboard/README.md#how-to-share-a-dashboard
[How to import/export dashboards?]:how-to-dashboard/README.md#how-to-importexport-dashboards
[How to properly save a new visualization or dashboard?]:how-to-dashboard/README.md#how-to-properly-save-a-new-visualization-or-dashboard
[How to edit a visualization?]:how-to-dashboard/README.md#how-to-edit-a-visualization
[How to edit the name of a visualization?]:how-to-dashboard/README.md#how-to-edit-the-name-of-a-visualization
[How to create a visualization?]:how-to-dashboard/README.md#how-to-create-a-visualization
[How to edit a dashboard?]:how-to-dashboard/README.md#how-to-edit-a-dashboard
[How to create a dashboard?]:how-to-dashboard/README.md#how-to-create-a-dashboard
[How to change the top menu?]:how-to-dashboard/README.md#how-to-change-the-top-menu
[Author_org_name and author_org_name, what's the difference?]:how-to-dashboard/README.md#author_org_name-and-author_org_name-whats-the-difference
[How to format a date?]:how-to-dashboard/README.md#how-to-format-a-date
[How to clear the kibana cache?]:how-to-dashboard/README.md#how-to-clear-the-kibana-cache





