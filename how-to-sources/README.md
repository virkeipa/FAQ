## What is the projects.json?

The projects.json describes the repositories (grouped by project and data sources) to be shown in the dashboards. It is
composed of three levels:
- First level: project names
- Second level: data sources and metadata
- Third level: data source URLs

## What is the setup.cfg?
The setup file holds the configuration to arrange all process underlying GrimoireLab. It is composed of different sections
that target general settings, phases, data storage info (`es_collection` and `es_enrichment`), backend and study sections. Details
of each section are available at: https://github.com/chaoss/grimoirelab-sirmordred

## How do the setup.cfg and projects.json relate each other?
The setup.cfg and projects.json are related via the backend section names (setup.cfg) and the data source names (projects.json). For instance,
giving the snippet of setup.cfg and projects.json, their relation is based on the name `github:repo`.

**setup.cfg**
```
[github:repo]
raw_index = github_grimoirelab_stats-raw
enriched_index = github_grimoirelab_stats
category = repository
no-archive = true
sleep-for-rate = true
```

**projects.json**
```
   "github:repo": [
        "https://github.com/chaoss/grimoirelab-toolkit"
    ]
```

## How to add a new repository?

A new repository is added by including it in the projects.json. The example below depicts two possible scenarios:
1. the repository is included in an existing project
2. the repository is part of a new project

```
{
    "grimoirelab": {
        "git": [
            "https://github.com/chaoss/grimoirelab-toolkit",
            "https://github.com/chaoss/grimoirelab-graal" <-- 1.
        ]
    },
    "secret-project": { <-- 2.
        "git": [
            "https://github.com/chaoss/grimoirelab-graal"
        ]
    },
}
```

## How to add a new MBox repository?
In order to start tracking a MBox repository, it is needed to: 
- subscribe our bot `barnowl@bitergia.com` to the target mailing list
- update the projects.json as follows:
    ```buildoutcfg
    ...
        "mbox": [
            "https://my-fantastic-group /home/bitergia/mboxes/barnowl_my-fantastic-group",
            "https://my-amazing-group /home/bitergia/mboxes/barnowl_my-amazing-group"
        ]
    ```
- open an issue in your GitLab support tracker

## How to enrich only a part of the raw data?

Let's suppose we want to collect all data available on `gerrit.chaoss.org`, which includes several projects. However,
we are interested in enriching only the projects labeled with `perceval` and `graal`. In this case, we have to
add `gerrit.chaoss.org` to the section `unknown`, and then include `gerrit.chaoss.org` with the proper filters (`--filter-raw=data.project`)
in the project we want. Note that the filter is defined based on the data obtained from the original repository, thus in
the example below, we assume that the original data includes an attribute with path `data.project`.

```
{
    "chaoss": {
        "gerrit": [
            "gerrit.chaoss.org --filter-raw=data.project:perceval",
            "gerrit.chaoss.org --filter-raw=data.project:graal",
        ]
    },
    "unknown": {
        "gerrit": [
            "gerrit.chaoss.org"
        ]
    }
}
```

## How to keep a repository in the projects.json when it doesn't exist anymore in upstream?

The repositories that don't exist anymore in upstream, but are present in the projects.json can filtered by 
appending `--filter-no-collection=true`. The collection process will ignore them, preventing in this way to log
useless errors.

```
{
    "chaoss": {
        "github": [
            "https:/github.com/chaoss/grimoirelab-perceval --filter-no-collection=true",
            "https:/github.com/chaoss/grimoirelab-sirmordred"
        ]
    }
}
```

## How to add labels to repositories?

Labels can be added to single repositories in the projects.json by appending `--labels=[tagA, tagB, ...]`. Labels
will be shown in the enriched index, while the attribute will be visible in the index pattern (it may be needed to refresh the index pattern).

```
{
    "chaoss": {
        "github": [
            "https:/github.com/chaoss/grimoirelab-perceval --labels=[core]",
            "https:/github.com/chaoss/grimoirelab-sirmordred --labels=[core]",
            "https:/github.com/chaoss/grimoirelab-graal --labels=[incubation]"
        ]
    }
}
```

## How to enable the tracking of a Slack instance?

- In your Slack instance, click the workspace name in the top left corner.
- Select `invite people` from the menu, then click `Members`.
- Enter the email address `owlbot@bitergia.com`.
- Under `Default Channels`, click `Edit/add` to choose the channels where `owlbot` will be added.
- Send the invitation.
- Once we receive the invitation, we will request a token. Depending on your Slack settings, the token can be automatically generated or needs the admin's approval.__

## How to get the ID of Slack channel?

Access your Slack instance via browser and click on the specific channel.

The URL in your browser will change to something similar to `https://app.slack.com/client/TXXX/CYYY`.
`TXXX` is the team ID, while `CXXX` is the channel ID.

## How to enable the download of archives in Groups.io?

The permission to download archives can be enabled by checking the corresponding box within the `Admin>Settings>Message Policies` page. Note that if this box is checked, any subscriber can download the archives.

## Which are the data sources that not allow retrieving all their history?

### mbox
The past conversations are not retrieved, we can collect only the conversations going forward from the day when we started tracking the groups.

### slack
If you are using the free version, we can only retrieve the most recent 10,000 messages.
More info at https://slack.com/intl/en-es/help/articles/115002422943-Message-file-and-app-limits-on-the-free-version-of-Slack.
